namespace :dev do
  desc "Configura o ambiente de desenvolvimento"
  task setup: :environment do

    puts "Cadastrando as plataformas... "
    100.times do |i|
      Plataform.create!(
        description: Faker::Name.name
      )
    end

    puts "Plataformas cadastradas com sucesso!"

    ############

    puts "Cadastrando os games... "
    100.times do |i|
      Register.create!(
        description: Faker::Name.name,
        server: Faker::Date.between(from: 2.days.ago, to: Date.today),
        plataform: Plataform.all.sample
      )
    end

    puts "Games cadastrados com sucesso!"
  end

end
