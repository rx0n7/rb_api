class AddPlataformToRegister < ActiveRecord::Migration[6.0]
  def change
    add_reference :registers, :plataform, null: true, foreign_key: true
  end
end
