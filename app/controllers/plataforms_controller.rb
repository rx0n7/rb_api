class PlataformsController < ApplicationController
  before_action :set_plataform, only: [:show, :update, :destroy]

  # GET /plataforms
  def index
    @plataforms = Plataform.all

    render json: @plataforms
  end

  # GET /plataforms/1
  def show
    render json: @plataform
  end

  # POST /plataforms
  def create
    @plataform = Plataform.new(plataform_params)

    if @plataform.save
      render json: @plataform, status: :created, location: @plataform
    else
      render json: @plataform.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /plataforms/1
  def update
    if @plataform.update(plataform_params)
      render json: @plataform
    else
      render json: @plataform.errors, status: :unprocessable_entity
    end
  end

  # DELETE /plataforms/1
  def destroy
    @plataform.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plataform
      @plataform = Plataform.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def plataform_params
      params.require(:plataform).permit(:description)
    end
end
